# Testing Techniques Project

## Prerequisites
- Two Matrix accounts need to have been made in advance.

- These two accounts must exclusively have a DM channel with each other.

- Login credentials for two test accounts are provided in the `.env` file. Please do not abuse these accounts because we'd like to keep using them for testing purposes.

## Steps

- Clone the project `git clone https://gitlab.science.ru.nl/cfabritius/testing-techniques-project`.

- Run `npm install`.

- For assignment 2: Run using `node_modules/typescript/bin/tsc && node assignment2.js` (add `-h` to run in non-headless mode).

- For assignment 3 with TorXakis: Run `node_modules/typescript/bin/tsc && node assignment3_torxakis.js` first. After the connection is opened, run `torxakis matrix.txs` from the `/torxakis` folder in another terminal. Then enter `tester Matrix Sut` and test using `test <number>`. To test with the test purpose `purpNonText` included, instead execute `tester Matrix purpNonText Sut`.

- For assignment 3 with GraphWalker: Run `java -jar graphwalker-cli-4.3.2.jar online --port 7890` from within the `/graphwalker` folder in a terminal. After that, in a separate terminal, run `node_modules/typescript/bin/tsc && node assignment3_graphwalker.js` from the main root folder.
To inspect the model: Run `java -jar graphwalker-studio-4.3.2.jar` from within the `\graphwalker` folder and open `http://127.0.0.1:9090/studio.html` in the browser.