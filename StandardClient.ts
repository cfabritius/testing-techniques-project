import puppeteer from "puppeteer";
import { sleep, ClientAdapter } from "./helper.js";



const homepage = "https://app.element.io/#/login";

const username_selector       = "#mx_LoginForm_username";
const password_selector       = "#mx_LoginForm_password";
const submit_selector         = "input[type=submit]";
const reset_selector          = ".mx_SetupEncryptionBody_reset_link";
const confirm_reset_selector  = ".mx_AccessibleButton_kind_danger_outline";
const cancel_selector         = "button[data-testid='dialog-cancel-button']";
const confirm_cancel_selector = ".danger";
const input_field_selector    = ".mx_BasicMessageComposer_input";
const edit_field_selector     = "div[aria-label='Edit message']";
const DM_channel_loc          = {x: 240, y: 175};
const triple_dot_loc          = {x: 1890, y: 1055};
const sticker_loc             = {x: 1800, y: 890};
const location_loc            = {x: 1815, y: 1005};
const drop_pin_loc            = {x: 1700, y: 980};
const pin_loc                 = {x: 1770, y: 700};
const share_location_loc      = {x: 1715, y: 985};
const poll_loc                = {x: 1815, y: 965};



export class ElementAdapter extends ClientAdapter {
    async setup(browser: puppeteer.Browser, username: string, password: string): Promise<void> {
        //Open page
        this.page = await browser.newPage();
        await this.page.goto(homepage);
        await this.page.setViewport({width: 1920, height: 1080});
        //Login
        await this.type(username_selector, username);
        await this.type(password_selector, password);
        await this.click(submit_selector);
        //Traverse Element's myriad attempts to get the user to enable back-ups
        await this.click(reset_selector);
        await this.click(confirm_reset_selector);
        await this.click(cancel_selector);
        await this.click(confirm_cancel_selector);
        //Open DM channel
        await sleep(300);
        await this.page.mouse.click(DM_channel_loc.x, DM_channel_loc.y);
    
        console.log(`Logged in on Element as '${username}'.`);
    }

    async send_text(msg: string): Promise<void> {
        await this.page.bringToFront();
    
        await this.type(input_field_selector, msg);
        await this.page.keyboard.press("Enter");
    }

    async send_reply(): Promise<void> {
        await this.page.bringToFront();

        const messages = await this.get_handles(".mx_EventTile_line");
        const latest_message = messages.at(-1);
        await latest_message.hover();
        const reply_button = await latest_message.$("div[aria-label='Reply']");
        await reply_button.click();
        await sleep(300);
        await this.page.keyboard.type("My Reply"); //The text in the reply is irrelevant
        await this.page.keyboard.press("Enter");
    }
    
    async send_sticker(): Promise<void> {
        await this.page.bringToFront();

        await this.page.mouse.click(triple_dot_loc.x, triple_dot_loc.y);
        await this.page.mouse.click(sticker_loc.x, sticker_loc.y);
        await sleep(5000); //Load sticker menu
        //Assume that a sticker pack has already been added
        await this.page.mouse.click(sticker_loc.x, sticker_loc.y);
        await sleep(3000);
    }
    
    async edit_message(new_msg: string): Promise<void> {
        await this.page.bringToFront();
        //Send a message to edit
        //await this.type(input_field_selector, "a__z");
        //await this.page.keyboard.press("Enter");
        //await sleep(1000) //Wait for message to appear

        //Assume the previous message was a text message
        const messages = await this.get_handles(".mx_EventTile_line");
        const latest_message = messages.at(-1);
        await latest_message.hover();
        const edit_button = await latest_message.$("div[aria-label='Edit']");
        await edit_button.click();
        (await this.get_handles(edit_field_selector))[0].focus();
        //Select and remove the old text
        await this.page.keyboard.down("ControlLeft");
        await this.page.keyboard.press("a");
        await this.page.keyboard.up("ControlLeft");
        await this.page.keyboard.press("Backspace");
        //Type the new text
        await this.page.keyboard.type(new_msg);
        await this.page.keyboard.press("Enter");
    }
    
    async send_location(): Promise<void> {
        await this.page.bringToFront();

        await this.page.mouse.click(triple_dot_loc.x, triple_dot_loc.y);
        await this.page.mouse.click(location_loc.x, location_loc.y);
        await this.page.mouse.click(drop_pin_loc.x, drop_pin_loc.y);
        await sleep(2000); //Load map preview
        await this.page.mouse.click(pin_loc.x, pin_loc.y);
        await this.page.mouse.click(share_location_loc.x, share_location_loc.y);
    }
    
    async send_thread(msg: string): Promise<void> {
        await this.page.bringToFront();

        await this.type(input_field_selector, "First message");
        await this.page.keyboard.press("Enter");
        await sleep(3000); //Wait for message to appear
        const messages = await this.get_handles(".mx_EventTile_line");
        const latest_message = messages.at(-1);
        await latest_message.hover();
        const thread_button = await latest_message.$("div[aria-label='Reply in thread']");
        await thread_button.click();
        await sleep(300);
        await this.page.keyboard.type(msg);
        await this.page.keyboard.press("Enter");
        await sleep(3000); //Wait for message to appear
        await this.page.keyboard.type("Final Message");
        await this.page.keyboard.press("Enter");
        await this.click("div[data-testid='base-card-close-button']");
    }

    async send_poll(): Promise<void> {
        await this.page.bringToFront();

        await this.page.mouse.click(triple_dot_loc.x, triple_dot_loc.y);
        await this.page.mouse.click(poll_loc.x, poll_loc.y);
        await this.type("#poll-topic-input", "Poll Question");
        await this.type("#pollcreate_option_0", "First option");
        await this.type("#pollcreate_option_1", "Second option");
        await this.click("button[type=submit]");
    }

    async send_spoiler(): Promise<void> {
        await this.page.bringToFront();

        await this.type(input_field_selector, "/spoiler Hello, World!");
        await this.page.keyboard.press("Enter");
    }

    async send_poll_reply(): Promise<void> {
        await this.page.bringToFront();

        await this.send_poll();
        await sleep(1000); //Wait for message to appear
        await this.send_reply();
    }
}
