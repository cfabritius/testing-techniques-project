import puppeteer from "puppeteer";
import { ClientAdapter, EventType, sleep } from "./helper.js";

export class TestClientAdapter extends ClientAdapter {
    messages_selector: string;

    async latest_message(): Promise<string> {
        await this.page.bringToFront();
        await this.page.keyboard.press("PageDown");
        await sleep(3000);
    
        const messages = await this.get_innerHTML(this.messages_selector);
        return messages.at(-1);
    }

    async observe_text(msg: string): Promise<boolean> {
        const message = await this.latest_message();
        //Expected output: message with the right content
        return message.includes(msg);
    }

    async observe_reply(type: EventType, msg=""): Promise<boolean> {
        switch(type) {
            case "Message":  return await this.observe_text(msg);
            case "Location": return await this.observe_location();
            case "Sticker":  return await this.observe_sticker();
            case "Poll":     return await this.observe_poll();
            case "Spoiler":  return await this.observe_spoiler();
            case "Edit":     return await this.observe_edit_message(msg);
            case "Thread":   return await this.observe_thread(msg);
            default:         return true;
        }
    }
    
    async observe_sticker(): Promise<boolean> {
        const message = await this.latest_message();
        //Expected output: image representing sticker
        return message.includes("<img"); //look for an image tag
    }
    
    async observe_edit_message(new_msg: string): Promise<boolean> {
        const message = await this.latest_message();
        //Expected output: a new message with edited content
        return message.includes(new_msg);
    }
    
    async observe_location(): Promise<boolean> {
        const message = await this.latest_message();
        //Expected output: message containing coordinates for the location
        return message.includes("18.646245142670466") && message.includes("18.808593749998153");
    }
    
    async observe_thread(msg: string): Promise<boolean> {
        const message = await this.latest_message();
        //Expected output: message replying to the previous message in the thread
        return message.includes(msg);
    }

    async observe_poll(): Promise<boolean> {
        const message = await this.latest_message();
        //Expected output: the question and options of the poll in text form
        return message.includes("Poll Question") && message.includes("First option") && message.includes("Second option");
    }

    async observe_spoiler(): Promise<boolean> {
        const message = await this.latest_message();
        //Expected output: the text should not be directly visible
        //This is actually quite difficult to check programmatically,
        //so this code checks if the HTML contains some acknowledgement of the spoiler
        //('hid' catches both 'hide' and 'hidden')
        //of course this will cause problems if the spoiler text itself contains these phrases...
        return message.includes("spoiler") || message.includes("hid");
    }

    async observe_poll_reply(): Promise<boolean> {
        //Expected output: the reply should include the text from the poll
        return this.observe_reply("Poll");
    }
}



const cinny_homepage = "https://app.cinny.in/";

const cinny_username_selector = "input[name=username]";
const cinny_password_selector = "input[name=password]";
const cinny_submit_selector   = "button[type=submit]";
const cinny_sidebar_selector  = ".sidebar-avatar";
const cinny_messages_selector = "div[data-message-item]";

export class CinnyAdapter extends TestClientAdapter {
    async setup(browser: puppeteer.Browser, username: string, password: string): Promise<void> {
        this.messages_selector = cinny_messages_selector
        
        //Open page
        this.page = await browser.newPage();
        await this.page.goto(cinny_homepage);
        await this.page.setViewport({width: 1920, height: 1080});
        //Login
        await this.type(cinny_username_selector, username);
        await this.type(cinny_password_selector, password);
        await this.click(cinny_submit_selector);
        //Open DM channel
        const DM_button = (await this.get_handles(cinny_sidebar_selector))[1];
        await DM_button.click();

        //Click on the messages container, to focus
        await this.page.mouse.click(1000, 400);

        console.log(`Logged in on Cinny as '${username}'.`);
    }

    async latest_text_message() {
        const msg_html = await this.latest_message();
        const  matches = /_161nxveu">(.*?)<\/div>/g.exec(msg_html);
        return matches && matches.length > 0 ? matches[1] : "";
    }

    async observe_edit_message_contents(): Promise<string> {
        const msg_html = await this.latest_message();
        const  matches = /_161nxveu">(.*?)<span/g.exec(msg_html);
        return matches && matches.length > 0 ? matches[1] : "";
    }

    async observe_thread_message_contents(): Promise<string> {
        const msg_html = await this.latest_message();
        const  matches = /_1en4l6y2">(.*?)<\/p>/g.exec(msg_html);
        return matches && matches.length > 0 ? matches[1] : "";
    }
}



const hydrogen_homepage = "https://hydrogen.element.io/#/login";

const hydrogen_username_selector = "#username";
const hydrogen_password_selector = "#password";
const hydrogen_submit_selector   = "button[type=submit]";
const hydrogen_sidebar_selector  = ".RoomList > li";
const hydrogen_messages_selector = "ul > li.Timeline_message";

export class HydrogenAdapter extends TestClientAdapter {
    async setup(browser: puppeteer.Browser, username: string, password: string): Promise<void> {
        this.messages_selector = hydrogen_messages_selector;
        
        //Open page
        this.page = await browser.newPage();
        await this.page.goto(hydrogen_homepage);
        await this.page.setViewport({width: 1920, height: 1080});
        //Login
        await this.type(hydrogen_username_selector, username);
        await this.type(hydrogen_password_selector, password);
        await this.click(hydrogen_submit_selector);
        //Open DM channel
        const DM_button = (await this.get_handles(hydrogen_sidebar_selector))[0];
        await DM_button.click();

        console.log(`Logged in on Hydrogen as '${username}'.`);
    }
}
