import puppeteer from "puppeteer";
import "dotenv/config";
import { ElementAdapter } from "./StandardClient.js";
import { CinnyAdapter, HydrogenAdapter, TestClientAdapter } from "./TestClients.js";
import { sleep, test_result } from "./helper.js";


let standard_client: ElementAdapter;
let test_clients = new Map() as Map<string, TestClientAdapter>;


async function run_test(label: string, clients: string[], input_method_name: string, output_method_name: string, delay=1000) {
    await standard_client[input_method_name]("Hello, World!");

    await sleep(delay); //Wait for message to be received;

    for(const client_name of clients) {
        const client = test_clients.get(client_name);
        const result = await client[output_method_name]("Hello, World!") as boolean;
        console.log(test_result(result, client_name, label));
    }
}



async function main() {
    const headless = process.argv.length < 3 ? "new" : false;

    const browser = await puppeteer.launch({headless});

    standard_client = new ElementAdapter();
    test_clients.set("Cinny", new CinnyAdapter());
    test_clients.set("Hydrogen", new HydrogenAdapter());

    await standard_client.setup(browser, process.env["SOCK1_USERNAME"], process.env["SOCK1_PASSWORD"]);
    await test_clients.get("Cinny").setup(browser, process.env["SOCK2_USERNAME"], process.env["SOCK2_PASSWORD"]);
    await test_clients.get("Hydrogen").setup(browser, process.env["SOCK2_USERNAME"], process.env["SOCK2_PASSWORD"]);

    console.log();

    //Run the tests
    await run_test("Text"         , ["Cinny", "Hydrogen"], "send_text"      , "observe_text"        ); //Test if the testing is working
    await run_test("Edit Message" , [         "Hydrogen"], "edit_message"   , "observe_edit_message"); //Test Case 2
    await run_test("Sticker"      , [         "Hydrogen"], "send_sticker"   , "observe_sticker"     ); //Test Case 1
    await run_test("Location"     , ["Cinny", "Hydrogen"], "send_location"  , "observe_location"    ); //Test Case 3 & 4
    await run_test("Thread"       , ["Cinny", "Hydrogen"], "send_thread"    , "observe_thread"      ); //Test Case 5 & 6
    await run_test("Poll"         , ["Cinny", "Hydrogen"], "send_poll"      , "observe_poll"        ); //Test Case 7 & 8
    await run_test("Spoiler"      , [         "Hydrogen"], "send_spoiler"   , "observe_spoiler"     ); //Test Case 9
    await run_test("Poll Reply"   , ["Cinny", "Hydrogen"], "send_poll_reply", "observe_poll_reply"  ); //Test Case 10 & 11
    
    await browser.close();
}

main();
