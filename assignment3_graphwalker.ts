import puppeteer from "puppeteer";
import "dotenv/config";
import { ElementAdapter } from "./StandardClient.js";
import { CinnyAdapter } from "./TestClients.js";
import { WebSocket } from "ws";
import * as fs from "fs"
import { colour_text, get_event_type, sleep } from "./helper.js";

const PORT = 7890;

async function connect(): Promise<WebSocket> {
    return new Promise((resolve, reject) => {
        const ws: WebSocket = new WebSocket("ws://localhost:" + PORT);
        ws.on("open", () => {
            console.log("Connected!");
            resolve(ws);
        });
        ws.on("error", reject);
    });
}

async function send_and_receive(ws: WebSocket, message: object, command: string): Promise<object> {
    return new Promise((resolve, reject) => {
        ws.send(JSON.stringify(message));

        const message_handler = (data: Buffer) => {
            const response = JSON.parse(data.toString());
            if(response["command"] == "visitedElement") return; //ignore progress reports
            ws.off("message", message_handler);
            if(response["command"] == "issues") reject(response["issues"][0]);
            if(response["command"] != command) reject("Response for the wrong command");
            resolve(response);
        }
        
        ws.on("message", message_handler);

        setTimeout(() => {
            ws.off("message", message_handler);
            reject("Timed out waiting for response")
        }, 2000);
    });
}



async function start(ws: WebSocket, model_name: string, model_data: object): Promise<boolean> {
    const message = {};
    message["command"] = "start";
    message["gw3"] = model_data;
    message["gw3"]["name"] = model_name;

    const response = await send_and_receive(ws, message, "start");
    return response["success"];
}

async function hasNext(ws: WebSocket): Promise<boolean> {
    const message = {"command": "hasNext"};
    
    const response = await send_and_receive(ws, message, "hasNext");
    return response["hasNext"];
}

async function getNext(ws: WebSocket): Promise<string> {
    const message = {"command": "getNext"};

    const response = await send_and_receive(ws, message, "getNext");
    return response["name"];
}



async function handle_send_command(standard_client: ElementAdapter, command: string) {
    const event_type = get_event_type(command);
    
    console.log("Sending", event_type.toLowerCase());

    if(event_type == "Location") {
        await standard_client.send_location();
    } else if(event_type == "Sticker") {
        await standard_client.send_sticker();
    } else if(event_type == "Poll") {
        await standard_client.send_poll();
    } else if(event_type == "Spoiler") {
        await standard_client.send_spoiler();
    } else if(event_type == "Reply") {
        await standard_client.send_reply();
    } else if(event_type == "Edit") {
        await standard_client.edit_message("This is a new message");
    } else if(event_type == "Thread") {
        await standard_client.send_thread("Hello, World!");
    } else if(event_type == "Message") {
        await standard_client.send_text("Hello, World!");
    } else {
        console.log(`Unknown event type in SendEvent: "${command}"`);
    }
}

function output_observation(result: boolean, word: string) {
    if(result) {
        console.log(`Observed ${colour_text("valid", "green")} ${word.toLowerCase()}`);
    } else {
        console.log(`Observed ${colour_text("invalid", "red")} ${word.toLowerCase()}`);
    }
}

async function handle_observe_command(test_client: CinnyAdapter, previous_commands: string[]) {
    if(previous_commands.length == 0) return; //GraphWalker shouldn't get to this case

    const last_command_type = get_event_type(previous_commands.at(-1));
    let result = true;
    if(last_command_type == "Location") {
        result = await test_client.observe_location();
    } else if(last_command_type == "Sticker") {
        result = await test_client.observe_sticker();
    } else if(last_command_type == "Poll") {
        result = await test_client.observe_poll();
    } else if(last_command_type == "Spoiler") {
        result = await test_client.observe_spoiler();
    } else if(last_command_type == "Reply") {
        if(previous_commands.length < 2) return; //GraphWalker shouldn't get to this case

        const type = get_event_type(previous_commands.at(-2));
        result = await test_client.observe_reply(type, "Hello, World!");
    } else if(last_command_type == "Edit") {
        result = await test_client.observe_edit_message("This is a new message");
    } else if(last_command_type == "Thread") {
        result = await test_client.observe_thread("Hello, World!");
    } else { //plain message
        result = await test_client.observe_text("Hello, World!");
    }
    
    output_observation(result, last_command_type);
}



async function main() {
    const headless = process.argv.length < 3 ? "new" : false;

    const browser = await puppeteer.launch({headless});

    const standard_client = new ElementAdapter();
    const cinny = new CinnyAdapter();

    await standard_client.setup(browser, process.env["SOCK1_USERNAME"], process.env["SOCK1_PASSWORD"]);
    await cinny.setup(browser, process.env["SOCK2_USERNAME"], process.env["SOCK2_PASSWORD"]);

    console.log();
    
    //Connect to the model-based tester
    const contents = fs.readFileSync("./graphwalker/Matrix.json");
    const model = JSON.parse(contents.toString());
    
    const ws = await connect();
    
    const started = await start(ws, "Matrix model", model);
    if(!started) {
        console.error("Failed to start");
        ws.close();
        return;
    }

    let previous_commands: string[] = [];

    while(await hasNext(ws)) {
        const next_state = await getNext(ws);
        if(next_state.startsWith("v_")) continue;
        if(next_state.includes("SendEvent")) {
            await handle_send_command(standard_client, next_state);
            previous_commands.push(next_state);
            await sleep(5_000) //Wait for the Event to arrive on the test client
        } else if(next_state.includes("ObserveEvent")) {
            await handle_observe_command(cinny, previous_commands);
        }
    }

    ws.close();
    await browser.close();
}

main();