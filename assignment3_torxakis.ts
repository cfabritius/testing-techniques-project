import puppeteer from "puppeteer";
import "dotenv/config";
import { ElementAdapter } from "./StandardClient.js";
import { CinnyAdapter } from "./TestClients.js";
import * as net from "net"
import { get_event_type } from "./helper.js";

const PORT = 7890

function get_command_contents(command: string): string {
    const matches = /"(.*?)"/g.exec(command);
    if(!matches || matches.length < 2) return "";
    return matches[1];
}

function output_confirmation(socket: net.Socket) {
    return () => {
        console.log("Done");
        socket.write("Done\n");
    }
}

function output_observation(socket: net.Socket, word: string) {
    return (is_valid_fallback: boolean) => {
        if(is_valid_fallback) {
            console.log("Observed valid", word.toLocaleLowerCase());
            socket.write(`OutputObj(${word})\n`);
        } else {
            console.log("Observed invalid", word.toLocaleLowerCase());
            socket.write(`OutputObj(Bad${word})\n`);
        }
    }
}


function handle_send_command(socket: net.Socket, standard_client: ElementAdapter, input: string) {
    const event_type = get_event_type(input);
    
    if(event_type == "Location") {
        console.log("Sending location");
        standard_client.send_location().then(output_confirmation(socket));
    } else if(event_type == "Sticker") {
        console.log("Sending sticker");
        standard_client.send_sticker().then(output_confirmation(socket));
    } else if(event_type == "Poll") {
        console.log("Sending poll");
        standard_client.send_poll().then(output_confirmation(socket));
    } else if(event_type == "Spoiler") {
        console.log("Sending spoiler");
        standard_client.send_spoiler().then(output_confirmation(socket));
    } else if(event_type == "Reply") {
        console.log("Sending reply");
        standard_client.send_reply().then(output_confirmation(socket));
    } else if(event_type == "Edit") {
        const msg_content = get_command_contents(input);
        console.log(`Editing message: "${msg_content}"`);
        standard_client.edit_message(msg_content).then(output_confirmation(socket));
    } else if(event_type == "Thread") {
        const msg_content = get_command_contents(input);
        console.log(`Sending thread message: "${msg_content}"`);
        standard_client.send_thread(msg_content).then(output_confirmation(socket));
    } else if(event_type == "Message") {
        const msg_content = get_command_contents(input);
        console.log(`Sending message: "${msg_content}"`);
        standard_client.send_text(msg_content).then(output_confirmation(socket));
    } else {
        console.log(`Unknown event type in SendEvent: "${input}"`);
    }
}


function handle_observe_command(socket: net.Socket, test_client: CinnyAdapter, input: string, previous_commands: string[]) {
    if(previous_commands.length == 0) return; //quiessence
    
    const last_command_type = get_event_type(previous_commands.at(-1));
    if(last_command_type == "Location") {
        test_client.observe_location().then(output_observation(socket, "Location"));
    } else if(last_command_type == "Sticker") {
        test_client.observe_sticker().then(output_observation(socket, "Sticker"));
    } else if(last_command_type == "Poll") {
        test_client.observe_poll().then(output_observation(socket, "Poll"));
    } else if(last_command_type == "Spoiler") {
        test_client.observe_spoiler().then(output_observation(socket, "Spoiler"));
    } else if(last_command_type == "Reply") {
        if(previous_commands.length < 2) return; //quiessence

        const type = get_event_type(previous_commands.at(-2));
        const contents = get_command_contents(input);
        test_client.observe_reply(type, contents).then(output_observation(socket, "Reply"));
    } else if(last_command_type == "Edit") {
        test_client.observe_edit_message_contents().then(content => {
            console.log(`New contents: "${content}"`);
            socket.write(`OutputObj(EditMessage("${content}"))\n`);
        });
    } else if(last_command_type == "Thread") {
        test_client.observe_thread_message_contents().then(content => {
            console.log(`Thread message: "${content}"`);
            socket.write(`OutputObj(ThreadMessage("${content}"))\n`);
        });
    } else { //plain message
        test_client.latest_text_message().then(content => {
            console.log(`Latest message: "${content}"`);
            socket.write(`OutputObj(Message("${content}"))\n`);
        });
    }
}



async function main() {
    const headless = process.argv.length < 3 ? "new" : false;

    const browser = await puppeteer.launch({headless});

    const standard_client = new ElementAdapter();
    const cinny = new CinnyAdapter();

    await standard_client.setup(browser, process.env["SOCK1_USERNAME"], process.env["SOCK1_PASSWORD"]);
    await cinny.setup(browser, process.env["SOCK2_USERNAME"], process.env["SOCK2_PASSWORD"]);

    console.log();

    //Connect to the model-based testers
    const server = net.createServer(socket => {
        console.log("Connected!");
        let previous_commands: string[] = [];
        
        socket.on("data", data => {
            const input = data.toString().slice(0, -1);
            
            if(input.startsWith("SendEvent")) {
                handle_send_command(socket, standard_client, input);
                previous_commands.push(input);
            } else if(input.startsWith("ObserveEvent")) {
                handle_observe_command(socket, cinny, input, previous_commands);
            } else {
                console.log(`Unknown command: "${input}"`);
            }
        });

        socket.on("close", () => console.log("Disconnected."));
    });

    console.log(`Listening on port ${PORT}...`);
    server.listen(PORT);
}

main();
