import puppeteer from "puppeteer";

export type EventType = "Message"|"Location"|"Sticker"|"Poll"|"Spoiler"|"Reply"|"Edit"|"Thread"|"Unknown";

export function get_event_type(command: string): EventType {
    if(command.includes("Location")) {
        return "Location";
    } else if(command.includes("Sticker")) {
        return "Sticker";
    } else if(command.includes("Poll")) {
        return "Poll";
    } else if(command.includes("Spoiler")) {
        return "Spoiler";
    } else if(command.includes("Reply")) {
        return "Reply";
    } else if(command.includes("EditMessage")) {
        return "Edit";
    } else if(command.includes("ThreadMessage")) {
        return "Thread";
    } else if(command.includes("Message")) {
        return "Message";
    }
}

export function sleep(ms: number) {
    return new Promise(res => setTimeout(res, ms));
}

export function colour_text(text: string, colour: string) {
    const map = {"red": 31, "green": 32};
    if(colour in map) {
        return `\x1b[${map[colour]}m${text}\x1b[0m`;
    } else {
        return text;
    }
}

export function test_result(result: boolean, client: string, label: string) {
    const coloured_result = result ? colour_text("succeeded", "green") : colour_text("failed", "red");
    return `${client} test '${label}' ${coloured_result}.`;
}

export class ClientAdapter {
    page: puppeteer.Page;
    
    async setup(browser: puppeteer.Browser, username: string, password: string): Promise<void> {
        console.log("Did not implement setup method.");
    }

    async get_handles(selector: string) {
        await this.page.waitForSelector(selector);
        return await this.page.$$(selector);
    }

    async get_innerHTML(selector: string) {
        await this.page.waitForSelector(selector);
        return await this.page.$$eval(selector, xs => xs.map(x => x.innerHTML));
    }

    async type(selector: string, str: string) {
        const input_handle = await this.page.waitForSelector(selector);
        await input_handle.type(str);
    }

    async click(selector: string) {
        const button_handle = await this.page.waitForSelector(selector);
        await button_handle.click();
    }

    async focus(selector: string) {
        const element_handle = await this.page.waitForSelector(selector);
        await element_handle.focus();
    }
}
