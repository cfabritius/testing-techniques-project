-- This model will have torxakis connect to the SUT via localhost port 7890.

-- ----------------------------------------------------------------

TYPEDEF
    OutputObject ::=   Done
                     | OutputObj { val :: Event }
ENDDEF

-- ----------------------------------------------------------------

-- For now, Matrix messages are simply modelled as strings.
TYPEDEF
    Event ::=   Message { contents :: String }
              | EditMessage { newContents :: String }
              | ThreadMessage { tmsgContents :: String }
              | Location
              | Poll
              | Reply
              | Spoiler
              | Sticker
ENDDEF


-- ----------------------------------------------------------------

-- Chat history.
TYPEDEF
    EventHistory ::=   Nil
                     | Cons { hd :: Event
                            ; tl :: EventHistory
                            }
ENDDEF

-- ----------------------------------------------------------------

-- Operations that clients can perform.
TYPEDEF
    ClientOp ::=   SendEvent { event :: Event }
                 | ObserveEvent
ENDDEF

-- ----------------------------------------------------------------

-- Input and output channels (from the perspective of the SUT).
CHANDEF
    Channels ::=   In  :: ClientOp
                 ; Out :: OutputObject
ENDDEF

-- ----------------------------------------------------------------

-- Describes the type of the state and which transitions are possible.
STAUTDEF
    matrixStateAut [ Inp :: ClientOp ; Outp :: OutputObject ] () ::=
        STATE
            mstate,
            -- `mout` is a state from which you can only leave by giving the
            -- correct (i.e. expected) output.
            mout,
            -- From `mWaitDone`, wait until test adapter sends "done".
            mWaitDone
        VAR
            hist :: EventHistory ;
            sentTextMsg :: Bool
        INIT
            mstate { hist := Nil ; sentTextMsg := False }

        TRANS
            -- Send a message and prepend it to the chat history.
            mstate ->
                Inp ? clOp
                [[
                    IF isSendEvent(clOp) THEN
                        IF isMessage(event(clOp)) THEN
                            strinre(
                                contents(event(clOp)),
                                REGEX('[A-Za-z0-9 ,]*')
                            )
                        ELSE
                            False
                        FI
                    ELSE
                        False
                    FI
                ]]
                { hist := Cons(event(clOp), hist) ; sentTextMsg := True }
                -> mWaitDone

            -- Send command to edit a message and prepend it to the chat
            -- history.
            mstate ->
                Inp ? clOp
                [[
                    IF isSendEvent(clOp) THEN
                        IF isEditMessage(event(clOp)) THEN
                            strinre(
                                newContents(event(clOp)),
                                REGEX('[A-Za-z0-9 ,]*')
                            )
                            /\ sentTextMsg
                        ELSE
                            False
                        FI
                    ELSE
                        False
                    FI
                ]]
                { hist := Cons(event(clOp), hist) }
                -> mWaitDone

            -- Send reply to a message in a thread and prepend it to the chat
            -- history.
            mstate ->
                Inp ? clOp
                [[
                    IF isSendEvent(clOp) THEN
                        IF isThreadMessage(event(clOp)) THEN
                            strinre(
                                tmsgContents(event(clOp)),
                                REGEX('[A-Za-z0-9 ,]*')
                            )
                        ELSE
                            False
                        FI
                    ELSE
                        False
                    FI
                ]]
                { hist := Cons(event(clOp), hist) }
                -> mWaitDone

            -- Send something without contents and prepend it to the chat
            -- history.
            mstate ->
                Inp ? clOp
                [[
                    IF isSendEvent(clOp) THEN
                        IF     not(isMessage(event(clOp)))
                            /\ not(isEditMessage(event(clOp)))
                            /\ not(isThreadMessage(event(clOp)))
                        THEN
                            True
                        ELSE
                            False
                        FI
                    ELSE
                        False
                    FI
                ]]
                { hist := Cons(event(clOp), hist) }
                -> mWaitDone

            -- Wait until test adapter sends "Done" so we know it's ready.
            mWaitDone ->
                Outp ! Done
                -> mstate

            -- Try to observe an event when the chat history is empty: nothing
            -- happens.
            mstate ->
                Inp ? clOp
                [[ isObserveEvent(clOp) /\ isNil(hist) ]]
                -> mstate

            -- Try to observe an event when the chat history is not empty: go to
            -- `mout` from where we will do a check.
            mstate -> Inp ? clOp [[ isObserveEvent(clOp) /\ not(isNil(hist)) ]] -> mout
            -- Copy-paste it a few times to make it more common. (Torxakis only
            -- supports uniform distribution of transitions as far as I'm
            -- aware.)
            mstate -> Inp ? clOp [[ isObserveEvent(clOp) /\ not(isNil(hist)) ]] -> mout
            mstate -> Inp ? clOp [[ isObserveEvent(clOp) /\ not(isNil(hist)) ]] -> mout
            mstate -> Inp ? clOp [[ isObserveEvent(clOp) /\ not(isNil(hist)) ]] -> mout

            -- Observe the expected message and go back to normal/default state.
            mout ->
                Outp ! OutputObj(hd(hist))
                -> mstate
ENDDEF

-- ----------------------------------------------------------------

MODELDEF
    Matrix ::=
        CHAN IN In
        CHAN OUT Out
        BEHAVIOUR matrixStateAut [ In, Out ] ()
ENDDEF

-- ----------------------------------------------------------------

CNECTDEF
    Sut ::=
        CLIENTSOCK

        -- From tester POV: channel `In` is output and channel `Out` is input.
        CHAN OUT In HOST "localhost" PORT 7890
        CHAN IN Out HOST "localhost" PORT 7890

        -- Encode `clOp` for SUT adapter by converting to string.
        ENCODE In ? clOp -> ! toString(clOp)

        DECODE Out ! fromString(s) <- ? s
ENDDEF

-- ----------------------------------------------------------------

PROCDEF
    procNonText
        [ Inp :: ClientOp ; Outp :: OutputObject ]
        ( wantedEvent :: Event )
        HIT
    ::=
            Inp ? clOp
            [[
                IF isSendEvent(clOp) THEN
                    event(clOp) == wantedEvent
                    -- isLocation(event(clOp)) \/
                    -- isPoll(event(clOp) \/
                    -- isSticker(event(clOp))
                ELSE
                    False
                FI
            ]]
            >->
            Outp ! Done
            >->
            Inp ? clOp [[ isObserveEvent(clOp) ]]
            >->
            Outp ! OutputObj(wantedEvent)
            >-> HIT
        ##
            Inp ? clOp
            [[
                -- Disallow sending things which contain a random string because
                -- if torxakis afterwards sends `ObserveEvent`, the output won't
                -- match any of the transitions in this process and hence the
                -- process will "halt" without completing the goal. And I can't
                -- make it match a transition here because I can't type all
                -- possible values for the random strings.
                IF isSendEvent(clOp) THEN
                    not(isMessage(event(clOp))) /\
                    not(isEditMessage(event(clOp))) /\
                    not(isThreadMessage(event(clOp)))
                ELSE
                    True
                FI
            ]]
            >->
            procNonText [Inp, Outp] (wantedEvent)
        ##
            Outp ! Done >-> procNonText [Inp, Outp] (wantedEvent)
        ##
            Outp ! OutputObj(Location) >-> procNonText [Inp, Outp] (wantedEvent)
        ##
            Outp ! OutputObj(Poll) >-> procNonText [Inp, Outp] (wantedEvent)
        ##
            Outp ! OutputObj(Sticker) >-> procNonText [Inp, Outp] (wantedEvent)
ENDDEF

-- ----------------------------------------------------------------

PURPDEF
    purpNonText ::=
        CHAN IN In
        CHAN OUT Out

        GOAL sendLocation ::= procNonText [In, Out] (Location)
        GOAL sendPoll ::= procNonText [In, Out] (Poll)
        GOAL sendSticker ::= procNonText [In, Out] (Sticker)
ENDDEF

-- ----------------------------------------------------------------
